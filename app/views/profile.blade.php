@include('head-meta')
@include('header')

<!-- PROFILE PAGE  -->
<br>
<div class="container">
  <div class="row">
    <div class="col-lg-5 col-lg-offset-1" id="red-panel">
      <div class="floating-image">
        <img src="/frontend/img/olympic-playground.png" class="olympic-playground">
      </div>	
      <div class="profile text-center">
      	<img src="/frontend/img/user.png">
      	<div class="panel-title">
	        <h5>{{$data['name']}}</h5>
      	</div>
      </div>
      <div class="profile-desc">
      	<div class="row">
	      	<div class="col-lg-6">
				<p>Email<br><span>{{$data['email']}}</span><br><br></p>
				<p>Domisili<br><span>{{\Kota::find($data['id_kota'])['kota']}}</span><br><br></p>
	      	</div>
	      	<div class="col-lg-6">
	      		<p>Telepon<br><span>{{$data['phone']}}</span><br><br></p>
				<p>Video Unggah<br><span>{{count($stream)}} Video</span><br><br></p>
	      	</div>
	      </div>
      	</div>
      	<div class="row">
      		<div class="col-lg-12">
      			<table width="100%">
      				<tr>
      					<td><p>Nama Anak</p></td>
      					<td><p>Tanggal Lahir Anak</p></td>
      				</tr>
      				@foreach($anak as $ke => $val)
                    <tr>
      					<td><p><span>{{$val->child}}</span></p></td>
      					<td><p><span>{{$val->child_birthdate}}</span></p></td>
      				</tr>
                    @endforeach
      			</table>
      			<div class="clear"></div>
      		</div>
      	</div>
    </div>

    <div class="col-lg-5" id="white-panel-profile">
      
      <div class="panel-title-red">
        <h3>Video yang dikirim</h3>
      </div>
      <div class="row" id="gallery-profile">
         @foreach($stream as $key =>$val)
          <div class="col-lg-6">
              <div class="gallery" data-toggle="modal" data-target="#videoModal">
                <img src="/frontend/img/overlay-play-btn.png" class="overlay-play-btn">
                <img src="/frontend/img/video-thumbnail.jpg" class="video-thumbnail">
                <div class="video-desc">
                  <h4>{{$val->title}}</h4>
                  <p>{{$val->name}}</p>
                </div>
              </div>
          </div>
          @endforeach
      </div>
    </div>
  </div>
</div>
<div class="clear"></div>
<!-- Video Modal -->
<div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="videoModal">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="closed" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <div class="flowplayer">
          <video>
            <source type="video/mp4" src="/frontend/video/video.mp4">
          </video>
        </div>
        <br>
        <div class="panel-title-yellow">
          <h3>Video Title</h3>
          <p>oleh: Name</p>
        </div>
      </div>
      <div class="modal-footer">  
        <div class="row">
          <div class="col-lg-5 col-lg-offset-1 text-center">
            <div class="form-group text-center">
              <a href="success.html"><button type="button" class="btn btn-twitter">Bagikan di Twitter</button></a>
            </div>
          </div>
          <div class="col-lg-5 text-center">
            <div class="form-group text-center">
              <a href="success.html"><button type="button" class="btn btn-facebook">Bagikan di Facebook</button></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@include('modal')
@include('js-footer')
@include('footer')