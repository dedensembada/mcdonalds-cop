<script type="text/javascript" src="/library/jquery/js/jquery.min.js"></script>
<script src="/frontend/js/bootstrap.min.js"></script>
<script src="/frontend/flowplayer/flowplayer.min.js"></script>
<script src="/frontend/js/bootstrap-datepicker.js"></script>
<script>
	$('.datepicker').datepicker({
	    todayHighlight: true,
	    format: 'yyyy-mm-dd'
	});
	var limit = 3;
	$('input.single-checkbox').on('change', function(evt) {
	   if($(this).siblings(':checked').length >= limit) {
	       this.checked = false;
	   }
	});
</script>
<script>
  //Google Analytics
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-57355634-1', 'auto');
  ga('send', 'pageview');

</script>