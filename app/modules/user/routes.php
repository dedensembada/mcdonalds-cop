<?php
Route::group(array('prefix' => '/', 'before' => ''), function () {
    Route::get( "users", [
        "as" => "users",
        "uses" => 'App\Modules\User\Controllers\UserController@userList'
    ]);
    Route::get( "users/add", [
        "as" => "users.add",
        "uses" => 'App\Modules\User\Controllers\UserController@userListAdd'
    ]);
    Route::post( "users/store", [
        "as" => "users.store",
        "uses" => 'App\Modules\User\Controllers\UserController@userListStore'
    ]);
    Route::get( "users/edit/{id}", [
        "as" => "users.edit",
        "uses" => 'App\Modules\User\Controllers\UserController@userListEdit'
    ])->where(array('id' => '[0-9]+'));
    Route::post( "users/update/{id}", [
        "as" => "users.update",
        "uses" => 'App\Modules\User\Controllers\UserController@userListUpdate'
    ])->where(array('id' => '[0-9]+'));
    Route::get( "users/delete/{id}", [
        "as" => "users.delete",
        "uses" => 'App\Modules\User\Controllers\UserController@userListDelete'
    ])->where(array('id' => '[0-9]+'));
    Route::get( "users/active/{id}", [
        "as" => "users.active",
        "uses" => 'App\Modules\User\Controllers\UserController@userActive'
    ])->where(array('id' => '[0-9]+'));
    Route::get( "users/inactive/{id}", [
        "as" => "users.inactive",
        "uses" => 'App\Modules\User\Controllers\UserController@userInActive'
    ])->where(array('id' => '[0-9]+'));
    Route::get( "users/search", [
        "as" => "users.search",
        "uses" => 'App\Modules\User\Controllers\UserController@search'
    ]);
    Route::get( "users/export", [
        "as" => "users.export",
        "uses" => 'App\Modules\User\Controllers\UserController@export'
    ]);


    Route::any( "mcd/login", [
        "as" => "login.admin",
        "uses" => 'App\Modules\User\Controllers\UserLoginController@login'
    ]);

    Route::post( "login/post", [
        "as" => "login.post",
        "uses" => 'App\Modules\User\Controllers\UserLoginController@login'
    ]);
    Route::get( "logout", [
        "as" => "logout",
        "uses" => 'App\Modules\User\Controllers\UserLoginController@logout'
    ]);
});