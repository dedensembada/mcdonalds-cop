<?php

use Illuminate\Support\Facades\View;

class PrizeController extends \BaseController {

    public function __construct()
    {
        $this->setupData();
    }

    public function index(){
        return View::make("prize", array(
            'page_title'    => 'McDonald\'s Champions of Play 2016',
            'menu'          => 'prize',
        ));
    }
}