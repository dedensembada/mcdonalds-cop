@include('head-meta')
@include('header')

<!-- HOW TO PAGE  -->
<div class="container">
  <div class="row">
    <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 text-center">
      <div class="olympic-playground-mobile">
        <img src="/frontend/img/olympic-playground.png">
      </div>
    </div>
  </div>
  <br>
  <div class="row">
    <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1" id="white-panel">
      <div class="floating-image">
        <img src="/frontend/img/olympic-playground.png" class="olympic-playground">
        <img src="/frontend/img/cloud-1.png" class="cloud-1">
        <img src="/frontend/img/cloud-2.png" class="cloud-2">
        <img src="/frontend/img/cloud-3.png" class="cloud-3">
      </div>
      <div class="panel-title">
        <h3>Cara Ikutan</h3>
        <!-- <p>Unggah video Anda dan tunggu hingga penanda konfirmasi muncul.</p> -->
      </div>
      <div class="how-to-desc">
      	<div class="row">
      		<div class="col-lg-4 text-center">
				<img src="/frontend/img/how-to-1.png">
				Daftarkan diri Anda<br>dan Si Kecil
      		</div>
      		<div class="col-lg-4 text-center">
				<img src="/frontend/img/how-to-2.png">
				Rekam video aksi keceriaan<br>Si Kecil bermain dengan bola<br>dan ceritakan keinginannya<br>pergi ke <span>Olimpiade Brazil 2016*</span>.
      		</div>
      		<div class="col-lg-4 text-center">
				<img src="/frontend/img/how-to-3.png">
				Upload dan bagikan video<br>si Kecil ke media sosial.<br>20 finalis akan bertanding di Olympic Playground yang akan diadakan di <span>Jakarta, 21 dan 22 Mei 2016</span>.
      		</div>
      	</div>
      	<div class="row">
      		<div class="col-lg-10 col-lg-offset-1 text-center">
      			<div class="how-to-footer">
					*Main lebih seru dan perbesar kesempatan Si Kecil menang dengan Red Ball.<br>Dapatkan Red Ball dengan menambahkan Rp 40.909,- (belum termasuk pajak) disertai pembelian paket Happy Meal.
      			</div>
      		</div>
      	</div>
      </div>
    </div>
  </div>
</div>

@include('modal')
@include('js-footer')
@include('footer')