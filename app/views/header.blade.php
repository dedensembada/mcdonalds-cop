<!-- TOP MENU -->
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand navbar-brand-mobile" href="#"><img src="/frontend/img/logo-cop.png"></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li {{$menu=='home'?'class=active':""}} ><a href="{{url('/')}}">BERANDA</a></li>
                <li {{$menu=='how-to'?'class=active':""}}><a href="{{route('how-to')}}">CARA IKUTAN</a></li>
                <li ><a class="navbar-brand navbar-brand-desktop" href="#"><img src="/frontend/img/logo-cop.png"></a></li>
                <li {{$menu=='gallery'?'class=active':""}}><a href="{{route('gallery')}}">GALERI</a></li>
                <li {{$menu=='prize'?'class=active':""}}><a href="{{route('prize')}}">HADIAH</a></li>
            </ul>
            @if(isset(\Session::get('users')['id']))
            <!-- <ul class="nav navbar-nav navbar-right top-icon-profile">
                <li><a href="{{route('profile',array(base64_encode(\Session::get('users')['id'])))}}"><img src="/frontend/img/user.png"><br>Halo, {{\Session::get('users')['name']}}</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right top-icon-profile">
                <li><a href="{{route('log-out')}}">Keluar</a></li>
            </ul> -->
            @endif
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>