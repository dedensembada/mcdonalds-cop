<?php
Route::group(array('prefix' => '/', 'before' => 'auth'), function () {
    Route::any( "gallery-video", [
        "as" => "gallery-video",
        "uses" => 'App\Modules\Gallery\Controllers\GalleryController@home'
    ]);
    Route::any( "gallery/delete/{id}", [
        "as" => "gallery-video.delete",
        "uses" => 'App\Modules\Gallery\Controllers\GalleryController@delete'
    ]);
    Route::any( "gallery-video/active/{id}", [
        "as" => "gallery-video.active",
        "uses" => 'App\Modules\Gallery\Controllers\GalleryController@active'
    ]);
    Route::any( "gallery-video/inactive/{id}", [
        "as" => "gallery-video.inactive",
        "uses" => 'App\Modules\Gallery\Controllers\GalleryController@inactive'
    ]);
    Route::any( "gallery-video/add", [
        "as" => "gallery-video.add",
        "uses" => 'App\Modules\Gallery\Controllers\GalleryController@add'
    ]);
    Route::any( "gallery-video/export", [
        "as" => "gallery-video.export",
        "uses" => 'App\Modules\Gallery\Controllers\GalleryController@export'
    ]);
});
