<?php

use Illuminate\Support\Facades\View;

class HowToController extends \BaseController {

    public function __construct()
    {
        $this->setupData();
    }

    public function index(){

        return View::make("how-to", array(
            'page_title'    => 'McDonald\'s Champions of Play 2016',
            'title'         => 'Cara Ikutan',
            'menu'          => 'how-to',
        ));
    }
}