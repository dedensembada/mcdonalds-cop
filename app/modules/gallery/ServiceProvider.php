<?php
namespace App\Modules\Gallery;

class ServiceProvider extends \App\Modules\ServiceProvider {

    public function register()
    {
        parent::register('gallery');
    }

    public function boot()
    {
        parent::boot('gallery');
    }

}