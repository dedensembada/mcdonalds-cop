<?php

use Illuminate\Support\Facades\View;

class HomeController extends \BaseController {

    public function __construct()
    {
        //facebook app
        session_start();
        $this->app_id ='1697230303899844'; // APP ID TEST
        $this->app_secret='dcd450c085eeb91ead96d3e781cfa5b4';
        //$this->app_id ='370801453114507'; // APP ID ASLI
        //$this->app_secret='653c18d1132e9e6378c75895eee9b1dc';

        $this->default_graph_version = 'v2.5';
        $this->setupData();
    }

    public function index(){
        return View::make("index", array(
            'page_title'    => 'McDonald\'s Champions of Play 2016',
            'menu'          => 'home',
        ));
    }

    public function register(){
        return View::make("register", array(
            'page_title'    => 'McDonald\'s Champions of Play 2016',
            'menu'          => 'home',
            'kota'          => \Kota::lists('kota', 'id_kota')
        ));
    }

    public function saveRegister(){

            $validator = $this->getUsersValidator();
            if ($validator->passes()) {

                $params['provider']     = \Input::get('provider');
                $params['name']         = \Input::get('name');
                $params['email']        = \Input::get('email');
                $params['id_kota']      = \Input::get('kota');
                $params['phone']        = \Input::get('phone');
                $params['child']        = \Input::get('child');
                $params['child_birthdate']        = \Input::get('birhtdate_child');
                $params['child_gender']        = \Input::get('txtgender');
                if(empty(\Input::get('hobby'))){
                    $params['child_hobby']        = implode(",",\Input::get('txthobby'));
                }else{
                    $params['child_hobby']        = \Input::get('hobby');
                }
                $params['redball']        = \Input::get('txtredball');
                
                $data = \User::saveData($params);
                \Session::put('users', $data);
                if($data){
                    return \Redirect::route('upload');
                }else{
                    return \Redirect::back()->withInput()->withErrors("Can't Input Data to Table");
                }
            } else {
                return \Redirect::back()->withInput()->withErrors($validator);
            }
    }

    protected function getUsersValidator()
    {
        return \Validator::make(\Input::all(), [
            "name"        => "required",
            "email"           => "required|email",
            "phone"           => "required",
            "child"           => "required",
            "txthobby"           => "required",
            "txtredball"           => "required",
            "birhtdate_child"           => "required",
        ]);
    }
    public function upload(){
        return View::make("upload", array(
            'page_title'    => 'McDonald\'s Champions of Play 2016',
            'menu'          => 'home',
        ));
    }
    public function uploadApi(){
       try {
            $extension = \Input::file('file')->getClientOriginalExtension();
            $fileName=date("Y-m-d-H-i-s").'-'.rand(11111, 99999) . 'videos';

            $s3 = \AWS::get('s3');

            $s3->putObject(array(
                'Bucket' => 'video.mcdonaldsredball.com/video',
                'Key' => $fileName.'.'.$extension,
                'SourceFile' => $_FILES['file']['tmp_name'],
            ));
            $data['file']=$fileName;
           $data['ext']=$extension;

        }catch (\Exception $e){
            $data['message']="Cannot upload to aws";
            return \Response::json($data, 400);
        }
        return \Response::json($data, 200);
    }
    public function uploadSave(){
        $validator = $this->getUploadValidator();
        if ($validator->passes()) {
            $params['title']         = \Input::get('title');
            $params['user_id']         = \Session::get('users')['id'];
            $params['video']         = \Input::get('file-video');
            $params['image']         = \Input::get('file-video').".jpg";
            $ext         = \Input::get('ext');
        try{
            //set zencoder
            $array=array(
                "input" => "https://s3-ap-southeast-1.amazonaws.com/video.mcdonaldsredball.com/video/".$params['video'].'.'.$ext,
                "outputs" => array(
                    array(
                        "skip" => array(
                            "max_duration"=>70
                        ),
                        "public" => "true",
                        "label" => "web",
                        "url"=> "https://s3-ap-southeast-1.amazonaws.com/video.mcdonaldsredball.com/output/".$params['video'].".mp4",
                        "thumbnails" => array(
                            "public"   => "true",
                            "base_url" => "https://s3-ap-southeast-1.amazonaws.com/video.mcdonaldsredball.com/thumbnail/",
                            "width"    => 800,
                            "height"   => 600,
                            "format"   => "jpg",
                            "filename" => $params['video']
                        ),
                        "notifications" => array(
                            "sidechainanalytics@gmail.com"
                        )
                    )
                )
            );
            \Zencoder::jobs()->create($array);
        } catch (Services_Zencoder_Exception $e) {
        }

            $data = \Gallery::saveData($params);
            if($data){
                return \Redirect::route('success',array($data['id']));
            }else{
                return \Redirect::back()->withInput()->withErrors("Can't Input Data to Table");
            }
        } else {
            return \Redirect::back()->withInput()->withErrors($validator);
        }
    }
    protected function getUploadValidator()
    {
        return \Validator::make(\Input::all(), [
            "title"         => "required",
            "file-video"          => 'required'
        ]);
    }

    public function success($id){
        $data = \Gallery::find($id);
        return View::make("success", array(
            'page_title'    => 'McDonald\'s Champions of Play 2016',
            'menu'          => 'home',
            'data'          => $data
        ));
    }

    public function detail($id){
        $data = \Gallery::find($id);
        return View::make("detail", array(
            'page_title'    => 'McDonald\'s Champions of Play 2016',
            'menu'          => 'home',
            'data'          => $data
        ));
    }

    public function profile($id){
        $data = \User::find(base64_decode($id));

        $stream=\Gallery::join('users', 'gallery.user_id', '=', 'users.id')
            ->where('gallery.is_deleted','=','0')
            ->where('gallery.published','=','1')
            ->where('users.email','=',$data['email'])
            ->select('gallery.*','users.name','users.email','users.child','users.child_birthdate','users.id_kota')
            ->orderBy('created_at','DESC')
            ->get();

        $anak=\User::where('users.email','=',$data['email'])
            ->select('users.child','users.child_birthdate')
            ->distinct()
            ->orderBy('created_at','DESC')
            ->get();

        return View::make("profile", array(
            'page_title'    => 'McDonald\'s Champions of Play 2016',
            'menu'          => 'home',
            'stream'       => $stream,
            'data'          => $data,
            'anak'          => $anak
        ));
    }
    public function login(){
        try{
            $fb = new Facebook\Facebook([
                'app_id' => $this->app_id, // Replace {app-id} with your app id
                'app_secret' => $this->app_secret,
                'default_graph_version' => $this->default_graph_version,

            ]);

            $helper = $fb->getRedirectLoginHelper();

            $permissions = ['email','user_birthday','public_profile']; // Optional permissions
            $loginUrl = $helper->getLoginUrl(route('login.callback'), $permissions);

            return \Redirect::to($loginUrl);

        }catch (\Exception $e){

            return \Redirect::back()->withErrors("Login failed");
        }
    }

    public function login_callback(){

        $fb = new Facebook\Facebook([
            'app_id' => $this->app_id, // Replace {app-id} with your app id
            'app_secret' => $this->app_secret,
            'default_graph_version' => $this->default_graph_version,

        ]);

        $helper = $fb->getRedirectLoginHelper();

        try {
            $accessToken = $helper->getAccessToken();
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            return \Redirect::to('/');
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            return \Redirect::to('/');
        }

        if (! isset($accessToken)) {
            if ($helper->getError()) {
                return \Redirect::to('/');
            } else {
                return \Redirect::to('/');
            }
            exit;
        }

        // The OAuth 2.0 client handler helps us manage access tokens
        $oAuth2Client = $fb->getOAuth2Client();

        // Get the access token metadata from /debug_token
        $tokenMetadata = $oAuth2Client->debugToken($accessToken);
        // Validation (these will throw FacebookSDKException's when they fail)
        $tokenMetadata->validateAppId($this->app_id); // Replace {app-id} with your app id

        $tokenMetadata->validateExpiration();

        if (! $accessToken->isLongLived()) {
            // Exchanges a short-lived access token for a long-lived one
            try {
                $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                return \Redirect::to('/');
            }
        }
        \Session::put('fb_access_token', (string) $accessToken);

        $fb->setDefaultAccessToken($accessToken);

        try {
            // Returns a `Facebook\FacebookResponse` object
            $response = $fb->get('/me?fields=id,name,birthday,email,picture.type(normal),gender,first_name,last_name', $accessToken);
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            return \Redirect::to('/');
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            return \Redirect::to('/');
        }

        $data = $response->getGraphUser();
        return View::make("register", array(
            'page_title'    => 'McDonald\'s Champions of Play 2016',
            'menu'          => 'home',
            'kota'          => \Kota::lists('kota', 'id_kota'),
            'data'          => $data
        ));

    }

    public function logout(){
        \Session::flush();
        return \Redirect::to("/");
    }
}