@extends("layout::main-layout")
@section("stylesheet_header")
@stop

@section("content")
    <div class="row panel panel-default">
        <div class="col-md-12">
            <div class="col-md-4">
               <a href="{{route('users')}}"> User Management </a>
            </div>
            <div class="col-md-4">
                <a href="{{route('gallery-video')}}"> Gallery Management </a>
            </div>
            <div class="col-md-4">
                <a href="{{route('howto')}}"> How To Page</a>
            </div>
        </div>
    </div>
@stop

@section("javascript_header")
@stop

@section("javascript_footer")

@stop