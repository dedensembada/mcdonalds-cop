<?php
Route::group(array('prefix' => '/', 'before' => 'auth'), function () {
    Route::any( "howto", [
        "as" => "howto",
        "uses" => 'App\Modules\Howto\Controllers\HowtoController@home'
    ]);
    Route::any( "howto/save/{id}", [
        "as" => "howto.save",
        "uses" => 'App\Modules\Howto\Controllers\HowtoController@save'
    ]);
});