@include('head-meta')
@include('header')

<!-- PRIZE PAGE  -->
<div class="container">
  <div class="row">
    <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 text-center">
      <div class="olympic-playground-mobile">
        <img src="/frontend/img/olympic-playground.png">
      </div>
    </div>
  </div>
  <br>
  <div class="row">
    <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1" id="white-panel">
      <div class="floating-image">
        <img src="/frontend/img/olympic-playground.png" class="olympic-playground">
        <img src="/frontend/img/cloud-1.png" class="cloud-1">
        <img src="/frontend/img/cloud-2.png" class="cloud-2">
        <img src="/frontend/img/cloud-3.png" class="cloud-3">
      </div>
      <div class="panel-title">
        <h3>Hadiah</h3>
        <!-- <p>Unggah video Anda dan tunggu hingga penanda konfirmasi muncul.</p> -->
      </div>
      <div class="how-to-desc">
      	<div class="row">
      		<div class="col-lg-4 col-lg-offset-2 text-center">
    				<img src="/frontend/img/prize-1.png">
    				1 anak laki-laki dan 1 anak perempuan akan menjadi duta Indonesia dalam upacara pembukaan<br><span>Olimpiade 2016 di Brazil</span>.
      		</div>
          <div class="col-lg-4 text-center">
            <img src="/frontend/img/prize-2.png">
            20 Finalis lainnya akan mendapatkan seragam jersey dan tas olahraga.<br>Dan Hadiah hiburan lainnya.
          </div>
      	</div>
      	<div class="row">
      		<div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 text-center">
            <br><br><img src="/frontend/img/olimpiade.png"><br><br>
          </div>
      	</div>
		    	
      </div>
    </div>
  </div>
</div>

@include('modal')
@include('js-footer')
@include('footer')