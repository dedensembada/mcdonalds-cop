<aside class="sidebar sidebar-left sidebar-menu">
    <!-- START Sidebar Content -->
    <section class="content slimscroll">
        <h5 class="heading">Main Menu</h5>
        <!-- START Template Navigation/Menu -->
        <ul class="topmenu topmenu-responsive" data-toggle="menu">
            <li class="{{$menu=='home'?'active open':''}}">
                <a href="{{route('dashboard')}}">
                    <span class="figure"><i class="ico-home2"></i></span>
                    <span class="text">Dashboard</span>
                </a>
            </li>
        </ul>
        <ul class="topmenu topmenu-responsive" data-toggle="user">
            <li class="{{$menu=='user'?'active open':''}}">
                <a href="{{route('users')}}">
                    <span class="figure"><i class="ico-people"></i></span>
                    <span class="text">User</span>
                </a>
            </li>
        </ul>
        <ul class="topmenu topmenu-responsive" data-toggle="gallery">
            <li class="{{$menu=='gallery'?'active open':''}}">
                <a href="{{route('gallery-video')}}">
                    <span class="figure"><i class="ico-bubble-video-chat"></i></span>
                    <span class="text">Video Management</span>
                </a>
            </li>
        </ul>

        <ul class="topmenu topmenu-responsive" data-toggle="howto">
            <li class="{{$menu=='howto'?'active open':''}}">
                <a href="{{route('howto')}}">
                    <span class="figure"><i class="ico-info"></i></span>
                    <span class="text">How To Page</span>
                </a>
            </li>
        </ul>
        <!--/ END Template Navigation/Menu -->
    </section>
    <!--/ END Sidebar Container -->
</aside>