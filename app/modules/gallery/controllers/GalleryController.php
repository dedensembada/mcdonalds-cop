<?php
namespace App\Modules\Gallery\Controllers;

use Illuminate\Support\Facades\View;


class GalleryController extends \BaseController {

    public function __construct()
    {
       	$this->setupData(); 
    }

    public function home(){
        $data=\Gallery::whereIsDeleted(0)->orderBy('id','DESC')->paginate(10);

        return View::make("gallery::gallery", array(
            'page_title'    => 'Gallery Management',
            'menu'          => "gallery",
            'data'          => $data
        ));
    }
    public function delete($id){

        $delete=\Gallery::deleteData($id);
        if($delete) {
            return \Redirect::to('gallery-video')->with('message', 'Gallery Deleted');
        }else{
            return \Redirect::to('gallery-video')->withError('message', 'Cannot Deleted');
        }
    }

    public function active($id){

        $status=\Gallery::activeData($id);
        if($status) {
            return \Redirect::to('gallery-video')->with('message', 'Gallery Active');
        }else{
            return \Redirect::to('gallery-video')->withError('message', 'Cannot Process');
        }
    }

    public function inactive($id){

        $status=\Gallery::inactiveData($id);
        if($status) {
            return \Redirect::to('gallery-video')->with('message', 'Gallery Inactive');
        }else{
            return \Redirect::to('gallery-video')->withError('message', 'Cannot Process');
        }
    }

    public function export(){
        $data=\Gallery::whereIsDeleted(0)->orderBy('id','DESC')->get();

        try{
            \Excel::create('gallery', function($excel) use($data) {

                $excel->sheet('Data Gallery', function($sheet) use($data) {

                    $sheet->fromArray($data);

                });

            })->export('xls');
        }catch (\Exception $e){
            return \Response::json($e->getMessage(), 400)->header('Access-Control-Allow-Origin','*');
        }
    }
}