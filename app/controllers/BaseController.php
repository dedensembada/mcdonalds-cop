<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	var $data = null;
	 public function setupData()
    {
        $this->data['page_title'] = "McDonald's Champions of Play 2016";
        $this->data['site_name'] = "";

    }


	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

    protected function isPostRequest()
    {
        return Input::server("REQUEST_METHOD") == "POST";
    }

    public function setPageTitle($pageTitle){
        $this->data['page_title'] = $pageTitle;
    }

    public function setPageMenu($menu){
        if(empty($menu)){
            $this->data['menu'] = 'dashboard';
        }else{
            $this->data['menu'] = $menu;
        }
    }

    public function test(){
        return View::make("test", array(
        ));
    }
    public function testSave(){
        $s3 = \AWS::get('s3');

        $s3->putObject(array(
            'Bucket'     => 'video.mcdonaldsredball.com/video',
            'Key'        => $_FILES['file']['name'],
            'SourceFile' => $_FILES['file']['tmp_name'],
        ));
    }

}
