<?php

use Illuminate\Support\Facades\View;

class GalleryController extends \BaseController {

    public function __construct()
    {
        $this->setupData();
    }

    public function index(){
        $search=\Input::get('search');
        $data=\Gallery::join('users', 'gallery.user_id', '=', 'users.id')
            ->where('gallery.is_deleted','=','0')
            ->where('gallery.published','=','1')
            ->where('users.name','like','%'.$search.'%')
            ->orWhere('users.child','like','%'.$search.'%')
            ->orWhere('gallery.title','like','%'.$search.'%')
            ->select('gallery.*','users.name')
            ->orderBy('created_at','DESC')
            ->paginate(9);

        return View::make("gallery", array(
            'page_title'    => 'McDonald\'s Champions of Play 2016',
            'menu'          => 'gallery',
            'data'          => $data
        ));
    }

    public function galleryDetail($id){
        try{

            $data=\Gallery::join('users', 'gallery.user_id', '=', 'users.id')
                ->where('gallery.is_deleted','=','0')
                ->where('gallery.published','=','1')
                ->where('gallery.id','=',$id)
                ->select('gallery.*','users.name')
                ->first();
            \Session::put('detail-id', $id);

            return \Response::json($data, 200);
        } catch (\Exception $e) {
            return \Response::json($e->getMessage(), 400);
        }
    }
}