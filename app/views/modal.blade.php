<!-- Video Home Modal -->
<div class="modal fade" id="videoHomeModal" tabindex="-1" role="dialog" aria-labelledby="videoHomeModal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="closed" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Champions of Play McDonald's Indonesia</h4>
            </div>
            <div class="modal-body">
                <!-- <iframe src="https://www.youtube.com/embed/6CEyu2LW9D0?rel=0&amp;showinfo=0;" frameborder="0" allowfullscreen></iframe> -->
                <div class="flowplayer">
                  <video>
                    <!-- <source type="video/mp4" src="/frontend/video/video-cop-1.mp4"> -->
                    <source type="video/mp4" src="https://s3-ap-southeast-1.amazonaws.com/video.mcdonaldsredball.com/video/video-cop-1.mp4">
                  </video>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Tnc Modal -->
<div class="modal fade" id="tncModal" tabindex="-1" role="dialog" aria-labelledby="videoHomeModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="closed" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Syarat dan Ketentuan</h4>
            </div>
            <div class="modal-body">
                <ul>
                    <li>Usia anak 8 – 11 tahun</li>
                    <li>Warga Negara Indonesia</li>
                    <li>Bersedia berkompetisi di babak final yang diadakan di Jakarta, 21 dan 22 Mei 2016. Akomodasi anak dan 1 (satu) orang pendamping ke dan di Jakarta ditanggung oleh McDonald’s.</li>
                    <li>Finalis terdiri dari adalah 14 pemilik Red Ball dan 6 non pemilik Red Ball</li>
                    <li>Video tidak boleh mengandung: SARA, pornografi dan hal-hal asusila, hal-hal yang tidak menyenangkan bagi pihak tertentu, pesan politik, brand lain selain McDonald’s, pelanggaran hak cipta, data pribadi (nomor telepon, email, dsb)</li>
                    <li>Video yang telah diunggah akan melalui proses moderasi selama maksimal 2 x 24 jam sebelum video ditampilkan di halaman Galeri.</li>
                    <li>Penentuan finalis dan pemenang dapat dibatalkan apabila tidak mencantumkan informasi data diri yang akurat dan atau tidak dapat dihubungi oleh pihak McDonald’s Indonesia dalam 3 (tiga) hari</li>
                    <li>Kompetisi ditutup pada 25 April 2016</li>
                    <li>Pengumuman finalis akan dilakukan pada 13 Mei 2016 di <a href="http://mcdonaldsredball.com/">www.mcdonaldsredball.com</a> dan <a href="http://mcdonalds.co.id" target="_blank">www.mcdonalds.co.id</a></li>
                    <li>Berkompetisi secara sportif</li>
                    <li>Keputusan juri adalah mutlak dan tidak dapat diganggu gugat</li>
                    <li>Hati-hati penipuan. Kompetisi ini tidak dipungut biaya apapun.</li>
                    <li>Dengan mengikuti kompetisi ini, maka seluruh peserta menyetujui semua persyaratan yang berlaku</li>
                </ul>
            </div>
        </div>
    </div>
</div>

<!-- Pvp Modal -->
<div class="modal fade" id="pvpModal" tabindex="-1" role="dialog" aria-labelledby="videoHomeModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="closed" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Kebijakan Privasi</h4>
            </div>
            <div class="modal-body">
                Terima kasih sudah mengunjungi situs kami <a href="http://mcdonaldsredball.com">www.mcdonaldsredball.com</a>, kami berupaya semampu kami untuk menjaga kepercayaan konsumen terhadap brand dan komitmen kami terhadap kerahasiaan Anda. Kami memiliki komitmen untuk melindungi dan menjaga kerahasiaan konsumen di internet, khususnya untuk anak-anak.<br><br>

Sekali lagi terima kasih telah mengunjungi situs kami dan atas kepercayaan Anda kepada McDonald's.<br><br>

Kebijakan Kerahasiaan Internet McDonald's<br>
McDonald's dipersilahkan untuk menyediakan informasi kepada semua konsumennya tentang kebijakan kerahasiaan online-nya. McDonald's berupaya sebaik-baiknya untuk melindungi kerahasiaan pengunjung situsnya.<br><br>

Jenis-jenis Informasi yang Dikumpulkan dan Penggunaannya<br>
McDonald's hanya menyimpan data pribadi seperti nama, alamat rumah, dan alamat email yang diserahkan secara sukarela. Sebagai contoh : data pribadi akan diminta ketika Anda menyampaikan saran, kritik dan atau feedback atas website atau produk kami, atau ketika Anda mendaftar ke dalam sebuah kontes/undian yang kami selenggarakan.<br><br>

Berbagi Data Pribadi<br>
McDonald's akan berbagi data pribadi kepada aliansi McDonald's, meliputi McDonald's Corporation, pihak franchise, anak perusahaan dan afiliasinya. Adapun informasi yang digunakan oleh McDonald's akan mengikuti kebijakan yang berlaku. Dalam periode waktu tertentu, kami akan mengirim informasi marketing kepada Anda seperti kupon diskon, informasi produk baru, dll. Apabila Anda tidak berkenan menerima kiriman tersebut, maka kami tidak akan mengirimkannya kepada Anda.<br><br>

Ada beberapa perusahaan yang terkait dengan McDonald's untuk memenuhi fungsi-fungsi tertentu, seperti : pesanan, membantu program promosi, penyediaan jasa teknisi untuk situs kami, dll. Perusahaan-perusahaan ini mungkin memiliki akses kepada data pribadi apabila dibutuhkan dalam melakukan pekerjaannya. Namun demikian, perusahaan ini hanya diperbolehkan menggunakan data-data tersebut untuk tujuan pekerjaan dan bukan untuk yang lain.<br><br>

McDonald's tidak menjual, mentransfer, atau menyebarluaskan data pribadi kepada pihak ketiga di luar aliansi McDonald's. Namun, dengan ijin Anda, kami secara berkala akan mengirimkan informasi marketing atas nama rekan bisnis kami perihal produk atau jasa yang mereka sediakan yang mungkin menarik minat Anda. Sebelumnya Anda akan ditanya apakah berkenan menerima materi dari rekan bisnis McDonald's. Jika Anda setuju menerima materi tersebut, McDonald's tidak akan memberikan data pribadi Anda kepada rekan bisnis McDonald's, tapi McDonald's akan mengirimkannya atas nama rekan bisnis McDonald's.<br><br>

McDonald's memiliki hak untuk menggunakan atau memberi informasi apapun yang diminta apabila dibutuhkan aparat hukum, untuk melindungi integritas situs, atas permintaan Anda, atau ketika dibutuhkan kerjasama dalam sebuah investigasi kepolisian atau keamanan publik.<br><br>

Akses<br>
Anda memiliki kuasa atas informasi/data yang Anda berikan kepada kami secara online. Apabila Anda bermaksud merubah data Anda di kontak kami, Anda dapat menghubungi kami via email atau alamat di bawah ini.<br><br>

Untuk melihat data pribadi Anda yang kami miliki atau untuk meminta kami menggunakan data tersebut, Anda bisa klik disini.<br><br>

Sambungan ke Situs Lain<br>
Kami mungkin menyediakan sambungan ke situs lain yang tidak dioperasikan oleh McDonald's Corporation. Apabila Anda mengunjungi situs tersebut, Anda perlu melihat kebijakan kerahasiaan yang mereka miliki. Kami tidak bertanggung jawab atas kebijakan dan praktik yang diterapkan perusahaan lain terhadap data pribadi yang Anda serahkan kepadanya.<br><br>

Situs McDonald's Lain<br>
Semua situs McDonald's yang dioperasikan oleh McDonald's Corporation akan mengikuti kebijakan kerahasiaan ini. Kebijakan beberapa situs McDonald's mungkin karena budaya lokal, hukum yang berlaku atau situasi yang ada. Namun demikian, situs yang diperasikan oleh McDonald's Corporation akan menghormati komitmen kepada konsumen kami terkait dengan kebijakan data pribadi dan penggunaannya.<br><br>

Perubahan dalam Kebijakan Kerahasiaan Kami<br>
Kebijakan kerahasiaan ini berlaku dari bulan Januari 2012. Seiring berjalannya waktu, mungkin akan ada beberapa hal yang dirasa perlu dirubah. Apabila kami merubah kebijakan kerahasiaan kami, kami akan menerbitkan revisinya disini. Kami sarankan untuk selalu melihat versi terbaru dari kebijakan kerahasiaan kami secara berkala. Namun, apapun perubahannya, tidak akan menurunkan standar kami dalam memperlakukan data pribadi yang sudah terkumpul.<br><br>

Apabila Anda memiliki pertanyaan tentang kebijakan kerahasiaan internet McDonald's, silahkan hubungi kami di:<br><br>

McDonald's Indonesia<br>
Gedung REKSO lantai 5<br>
Jl. Bulevar Artha GAding kav. A/1<br>
Sentra Bisnis Artha Gading<br>
Kelapa Gading – Jakarta 14240<br>
Indonesia
            </div>
        </div>
    </div>
</div>