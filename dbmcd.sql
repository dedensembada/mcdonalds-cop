-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 15 Mar 2016 pada 08.22
-- Versi Server: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbmcd`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `gallery`
--

CREATE TABLE IF NOT EXISTS `gallery` (
`id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `title` text,
  `caption` text,
  `image` varchar(255) DEFAULT NULL,
  `video` varchar(255) DEFAULT NULL,
  `published` tinyint(1) DEFAULT '0',
  `featured` tinyint(1) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data untuk tabel `gallery`
--

INSERT INTO `gallery` (`id`, `user_id`, `title`, `caption`, `image`, `video`, `published`, `featured`, `created_at`, `updated_at`, `is_deleted`) VALUES
(1, 1, 'The stupid man in the jungle', 'this is first caption of stupid man', NULL, 'stupid.video', 0, 0, '2016-03-13 19:14:54', '2016-03-13 19:14:54', 0),
(3, 1, 'Backspace in contenteditable fix', NULL, NULL, '1498420160315videos.mp4', 0, 0, '2016-03-15 06:13:31', '2016-03-15 06:12:19', 0),
(4, NULL, 'Backspace in contenteditable fix', NULL, NULL, '3905420160315videos.mp4', 0, 0, '2016-03-15 06:13:04', '2016-03-15 06:13:04', 0),
(5, 7, 'Backspace in contenteditable fix', NULL, NULL, '5027620160315videos.mp4', 0, 0, '2016-03-15 06:53:08', '2016-03-15 06:53:08', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `howto`
--

CREATE TABLE IF NOT EXISTS `howto` (
`id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `text` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `howto`
--

INSERT INTO `howto` (`id`, `title`, `text`, `created_at`, `updated_at`, `is_deleted`) VALUES
(1, 'Cara Ikutan', '&lt;p&gt;adasdad&lt;/p&gt;', '2016-03-13 18:28:38', '2016-03-13 18:28:38', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kota`
--

CREATE TABLE IF NOT EXISTS `kota` (
`id_kota` int(11) NOT NULL,
  `kota` varchar(255) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `kota`
--

INSERT INTO `kota` (`id_kota`, `kota`) VALUES
(1, 'Malang'),
(2, 'Surabaya'),
(3, 'Jakarta'),
(4, 'Bandung');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `provider` varchar(255) DEFAULT NULL,
  `provider_avatar` varchar(255) DEFAULT NULL,
  `provider_access_token` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `birthdate` datetime DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `id_kota` int(11) DEFAULT NULL,
  `child` varchar(255) DEFAULT NULL,
  `child_birthdate` timestamp NULL DEFAULT NULL,
  `child_gender` varchar(255) DEFAULT NULL,
  `child_hobby` varchar(255) DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `login_enabled` tinyint(1) DEFAULT '0',
  `deleted_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `remember_token` varchar(255) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `provider`, `provider_avatar`, `provider_access_token`, `username`, `password`, `name`, `email`, `phone`, `birthdate`, `gender`, `image`, `id_kota`, `child`, `child_birthdate`, `child_gender`, `child_hobby`, `last_login`, `login_enabled`, `deleted_at`, `created_at`, `updated_at`, `remember_token`, `is_deleted`) VALUES
(1, NULL, NULL, NULL, 'adminmcd', '$2y$10$9GqADpcVSVuFxeU6DGkRp.Z.TplljgUdygkj4j738fFj5B1qT21.G', 'administrator', '', '08563531001', NULL, NULL, '4405120160314photo.jpg', NULL, NULL, NULL, NULL, NULL, '2016-03-13 19:15:00', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-03-13 19:15:00', 'zLV8v1v0ewfjrwLFrQLfCuHwWDX29YdmDbJCPPqdQmBQzyE5orTyUjiQgVME', 0),
(2, NULL, NULL, NULL, 'luluq', '$2y$10$4j5W9.MQx/QGpcJ35CFFfudKDOmjOjxNr6Vx7JPT2Wgj/BTGhglJu', 'luluq miftakhul huda', 'luluq.ye@gmail.com', '08563531001', NULL, NULL, '5715520160314photo.jpg', NULL, NULL, NULL, NULL, NULL, '2016-03-13 18:39:15', 1, '0000-00-00 00:00:00', '2016-03-13 18:38:06', '2016-03-13 18:39:15', NULL, 1),
(3, NULL, NULL, NULL, NULL, NULL, 'luluq yek', 'asem@beritagar.id', '08563531001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-03-15 10:29:32', 0, '0000-00-00 00:00:00', '2016-03-15 10:29:32', '2016-03-15 10:29:32', NULL, 0),
(4, NULL, NULL, NULL, NULL, NULL, 'asdasd', 'asdasd@asdasd.dfg', '08563531001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-03-15 04:06:04', 0, '0000-00-00 00:00:00', '2016-03-15 04:06:04', '2016-03-15 04:06:04', NULL, 0),
(5, '1019597078112297', NULL, NULL, NULL, NULL, 'Luluq Miftakhul Huda', 'luluq.ye@gmail.com', '08563531001', NULL, NULL, NULL, 1, 'lili', '0000-00-00 00:00:00', 'on', 'Makan', '2016-03-15 06:47:47', 0, '0000-00-00 00:00:00', '2016-03-15 06:47:47', '2016-03-15 06:47:47', NULL, 0),
(6, '', NULL, NULL, NULL, NULL, 'luluq yek', 'luluq.ye@gmail.com', '08563531001', NULL, NULL, NULL, 1, 'lili', '1990-08-03 17:00:00', 'male', 'Makan', '2016-03-15 06:49:43', 0, '0000-00-00 00:00:00', '2016-03-15 06:49:43', '2016-03-15 06:49:43', NULL, 0),
(7, '', NULL, NULL, NULL, NULL, 'Adel', 'luluq.ye@gmail.com', '08563531001', NULL, NULL, NULL, 1, 'lili', '1990-08-03 17:00:00', 'male', 'olahraga', '2016-03-15 06:51:39', 0, '0000-00-00 00:00:00', '2016-03-15 06:51:39', '2016-03-15 06:51:39', NULL, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `howto`
--
ALTER TABLE `howto`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kota`
--
ALTER TABLE `kota`
 ADD PRIMARY KEY (`id_kota`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `howto`
--
ALTER TABLE `howto`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `kota`
--
ALTER TABLE `kota`
MODIFY `id_kota` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
