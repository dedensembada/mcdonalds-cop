<?php
namespace App\Modules\Howto\Controllers;

use Illuminate\Support\Facades\View;


class HowtoController extends \BaseController {

    public function __construct()
    {
       	$this->setupData(); 
    }

    public function home(){
        $data=\Howto::find(1);

        return View::make("howto::howto", array(
            'page_title'    => 'How To Page',
            'menu'          => "howto",
            'data'          => $data
        ));
    }

    public function save($id){
        if ($this->isPostRequest()) {
            $validator = $this->getValidator();
            if ($validator->passes()) {

                $input = \Input::all();

                $data=array(
                    "title"             =>  $input["title"],
                    "text"              =>  htmlspecialchars($input["text"])
                );

                $save=\Howto::updateData($id,$data);
                if($save) {
                    return \Redirect::to('howto')->with('message', 'How To page was update');
                }else{
                    return \Redirect::to('howto')->withError('message', 'Cannot Saved');
                }

            }else{
                return \Redirect::back()->withInput()->withErrors($validator);
            }
        }
    }

    protected function getValidator()
    {
        return \Validator::make(\Input::all(), [
            "title" => "required",
            "text" => "required",
        ]);
    }
}