<?php
namespace App\Modules\Howto;

class ServiceProvider extends \App\Modules\ServiceProvider {

    public function register()
    {
        parent::register('howto');
    }

    public function boot()
    {
        parent::boot('howto');
    }

}