@include('head-meta')
@include('header')
<!-- GALLERY PAGE  -->
<div class="container">
  <div class="row">
    <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 text-center">
      <div class="olympic-playground-mobile">
        <img src="/frontend/img/olympic-playground.png">
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-lg-offset-2">
      <div class="floating-image">
        <img src="/frontend/img/olympic-playground.png" class="olympic-playground">
        <img src="/frontend/img/cloud-1.png" class="cloud-1">
      </div>
      <div class="panel-title-red">
        <br>
        <h3>Galeri Video</h3>
      </div>
  </div>

  <div class="row">
    <div class="col-xs-8 col-xs-offset-1 col-lg-5 col-lg-offset-3 text-center">
      <form role="search">
        <div class="form-group">
          <input type="text" class="form-control-search" name="search" placeholder="Cari Video">
        </div>
      </form>
    </div>
    <div class="col-xs-1 col-lg-1">
      <img src="/frontend/img/icon-search.png" class="icon-search">
    </div>
  </div>
  
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10 col-lg-offset-1" id="red-panel">
      <div class="floating-image-gallery">
        <img src="/frontend/img/cloud-2.png" class="cloud-2">
        <img src="/frontend/img/cloud-3.png" class="cloud-3">
      </div>
      <br>
      <div class="row">
        @foreach($data as $key => $val)
          <div class="col-xs-6 col-lg-4">
            <div class="gallery" data-toggle="modal" data-id="{{$val->id}}" data-target="#videoModal">
              <img src="/frontend/img/overlay-play-btn.png" class="overlay-play-btn">
              <img src="https://s3-ap-southeast-1.amazonaws.com/video.mcdonaldsredball.com/thumbnail/{{$val->image}}" class="video-thumbnail">
              <div class="video-desc">
                <h4>{{$val->title}}</h4>
                <p>{{$val->name}}</p>
              </div>
            </div>
          </div>
        @endforeach
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-xs-10 col-xs-offset-1 col-lg-8 col-lg-offset-2 text-center">
      <nav>
        {{$data->links()}}
      </nav>
    </div>
  </div>

</div>
<div class="clear"></div>

<!-- Video Modal -->
<div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="videoModal">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="closed" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <div id="video"></div>
        <br>
        <div class="panel-title-yellow">
          <h3><span id="title"></span></h3>
          <p>oleh: <span id="name"></span></p>
        </div>
      </div>

      <div class="modal-footer">  
        <div class="row">
          <div class="col-lg-5 col-lg-offset-1 text-center">
            <div class="form-group text-center">
              <button onclick="window.open('https://twitter.com/intent/tweet?text=Anakku berkesempatan ke Olimpiade 2016 di Brazil dengan ikutan Champions of Play McDonald&#8217s&url={{route("detail",array(\Session::get('detail-id')))}}','twitter-share-dialog','width=auto,height=auto');return false;" type="button" class="btn btn-twitter">Bagikan di Twitter</button>
            </div>
          </div>
          <div class="col-lg-5 text-center">
            <div class="form-group text-center">
              <button onclick="window.open('https://www.facebook.com/sharer/sharer.php?u={{route("detail",array(\Session::get('detail-id')))}}','facebook-share-dialog','width=auto,height=auto');return false;" type="button" class="btn btn-facebook">Bagikan di Facebook</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

</div>
@include('modal')
@include('js-footer')
<script type="text/javascript">
  $('#videoModal').on('show.bs.modal', function(e) {

    //get data-id attribute of the clicked element
    var Id = $(e.relatedTarget).data('id');
    $.ajax({
      type: "GET",
      url: "/gallery/detail/"+Id,
      data: {
      },
      beforeSend: function(){
      },
      error: function(data){
        alert("error check message");
      },
      success: function( data )
      {
        var html= ' <div class="flowplayer" id="flow"><video>'+
                  '<source type="video/mp4" src="https://s3-ap-southeast-1.amazonaws.com/video.mcdonaldsredball.com/output/'+data.video+'.mp4">'+
                  '</video>  </div>';
        $('#video').html(html);
        $('#title').text(data.title);
        $('#name').text(data.name);
        $("#flow").flowplayer();
      }
    });
  });
</script>
@include('footer')