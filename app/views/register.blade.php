@include('head-meta')
@include('header')

<!-- REGISTER PAGE  -->
<div class="container">
  <div class="row">
    <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 text-center">
      <div class="olympic-playground-mobile">
        <img src="/frontend/img/olympic-playground.png">
        <br><br>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-8 col-lg-offset-2" id="red-panel">
      <div class="floating-image">
        <img src="/frontend/img/olympic-playground.png" class="olympic-playground">
        <img src="/frontend/img/cloud-1.png" class="cloud-1">
        <!-- <img src="/frontend/img/cloud-2.png" class="cloud-2"> -->
        <img src="/frontend/img/cloud-3.png" class="cloud-3">
      </div>
      <div class="panel-title">
        <h3>Registrasi</h3>
        <p>Isi biodata Anda selengkap mungkin, jangan sampai ada yang terlewat.</p>
      </div>
      <!-- Form horizontal layout bordered -->
      {{
            Form::open(array(
                'url'=> route('register.save'),
                'class' => '',
                'id'=>'form',
                'files' => true
            ))
       }}
      @if($errors->has())
        <div class="form-group">
          <div class="alert alert-danger fade in mt10">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4 class="semibold">Oh snap! You got an error!</h4>
            <p class="mb10">
              {{ HTML::ul($errors->all()) }}
            </p>
          </div>
        </div>
      @endif
      <div class="form-group">
        <label>Nama:</label>
        <input type="text" value="{{isset($data['name'])?$data['name']:\Session::get('users')['name']}}" name="name" class="form-control" placeholder="Tuliskan Nama Anda" required />
      </div>
      <div class="form-group">
        <label>Email:</label>
        <input type="email" value="{{isset($data['email'])?$data['email']:\Session::get('users')['email']}}" name="email" class="form-control" placeholder="Tuliskan Email Anda" required data-parsley-type="email" />
      </div>
      <div class="form-group">
        <label>Kota:</label>
        {{Form::select('kota', $kota,array(),array('class'=>'form-control'))}}
      </div>
      <div class="form-group">
        <label>No. Telp:</label>
        <input type="phone" name="phone" class="form-control" value="{{\Session::get('users')['phone']}}" placeholder="Tuliskan No Telp Anda" required data-parsley-type="digits">
      </div>
      <div class="form-group">
        <label>Nama Anak:</label>
        <input type="text" name="child" class="form-control" value="{{\Session::get('users')['child']}}" placeholder="Tuliskan Nama Anak Anda" required/>
      </div>
      <div class="form-group">
        <label>Tanggal Lahir Anak:</label>
        <input type="text" data-provide="datepicker" data-date-format="yyyy-mm-dd" name="birhtdate_child" class="form-control datepicker" value="{{\Session::get('users')['child_birthdate']}}" placeholder="Tuliskan Tanggal Lahir Anak Anda" required/>
       
      </div>

      <div class="form-group">
        <div class="row">
          <div class="col-lg-3"><label>Jenis Kelamin Anak:</label></div>
          <div class="col-lg-3"><label><input type="radio" value="male" name="txtgender" checked> Laki-laki</label></div>
          <div class="col-lg-3"><label><input type="radio" value="female" name="txtgender"> Perempuan</label></div>
        </div>
      </div>

      <div class="form-group">
        <div class="row">
          <div class="col-lg-3"><label>Hobi Anak:</label></div>
          <div class="col-lg-3"><label><input type="checkbox" class="txthobby" value="Olahraga" name="txthobby[]"/> Olahraga</label></div>
          <div class="col-lg-3"><label><input type="checkbox" class="txthobby" value="Menari" name="txthobby[]" /> Menari</label></div>
          <div class="col-lg-3"><label><input type="checkbox"  class="other" /> Lainnya:</label></div>
        </div>
        <div class="row">
          <div class="col-lg-3 col-lg-offset-3"><label><input class="txthobby" type="checkbox" value="Belajar" name="txthobby[]"> Belajar</label></div>
          <div class="col-lg-3"><label><input type="checkbox" class="txthobby" value="Berkebun" name="txthobby[]" /> Berkebun</label></div>
          <div class="col-lg-3"><label><input type="text" id="hobby" name="hobby" class="form-control" placeholder="Tuliskan Hobi Anda"/></label></div>
        </div>
        <div class="row">
          <div class="col-lg-3 col-lg-offset-3"><label><input class="txthobby" value="Membaca" type="checkbox" name="txthobby[]"> Membaca</label></div>
          <div class="col-lg-3"><label><input type="checkbox" class="txthobby" value="Memasak" name="txthobby[]" /> Memasak</label></div>
        </div>
        <div class="row">
          <div class="col-lg-3 col-lg-offset-3"><label><input class="txthobby" value="Main Game" type="checkbox" name="txthobby[]"> Main Game</label></div>
          <div class="col-lg-3"><label><input type="checkbox" class="txthobby" value="Menggambar" name="txthobby[]" /> Menggambar</label></div>
        </div>
      </div>
      <div class="form-group">
        <div class="row">
          <div class="col-lg-4"><label>Apakah kamu punya Red Ball:</label></div>
          <div class="col-lg-4"><label><input type="radio" value="yes" name="txtredball" checked> Ya</label></div>
          <div class="col-lg-4"><label><input type="radio" value="no" name="txtredball"> Tidak</label></div>
        </div>
        <div class="row">
          <div class="col-lg-4">&nbsp;</div>
          <div class="col-lg-4"><label>*Gunakan red ball dalam video untuk memperbesar peluang menang</label></div>
          <div class="col-lg-4"><label>*Gunakan red ball dalam video untuk memperbesar peluang menang. Dapatkan Red Ball di McDonald’s terdekat</label></div>
        </div>
      </div>
      <div class="form-group text-center">
        <input type="hidden" value="{{isset($data['id'])?$data['id']:""}}" name="provider"/>
        <button type="submit" class="btn btn-yellow">Lanjut</button>
      </div>
      {{ Form::close() }}
    </div>
  </div>

  <div class="step">
    <div class="row">
      <div class="col-xs-12 col-lg-6 col-lg-offset-3 text-center">
        <div class="row">
          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 active"><span class="active">1</span><br><br>Registrasi<img src="/frontend/img/dot.png" class="dot"></div>
          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"><span>2</span><br><br>Kirim Video<img src="/frontend/img/dot.png" class="dot"></div>
          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"><span>3</span><br><br>Selesai</div>
        </div>
      </div>
    </div>
  </div>

</div>
<div class="clear"></div>

@include('js-footer')
<script type="text/javascript">
  var limit = 4;
  $('.txthobby').on('change', function(evt) {
    if($('.txthobby:checked').length >= limit) {
      this.checked = false;
    }
  });
  $('.other').on('change', function(evt) {
    $('.txthobby').removeAttr('checked');
    $('')
  });
</script>
@include('footer')