<?php
Route::group(array('prefix' => '/', 'before' => 'auth'), function () {
    Route::get( "dashboard", [
        "as" => "dashboard",
        "uses" => 'App\Modules\Dashboard\Controllers\DashboardController@dashboard'
    ]);
    
});