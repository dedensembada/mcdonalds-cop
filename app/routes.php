<?php
Route::get('/', [
    'uses' => 'HomeController@index'
]);
Route::get('/login', [
    'as' => 'login',
    'uses' => 'HomeController@login'
]);
Route::get('/login_callback', [
    'as' => 'login.callback',
    'uses' => 'HomeController@login_callback'
]);
Route::get('/register', [
    'as' => 'register',
    'uses' => 'HomeController@register'
]);
Route::post('/register/save', [
    'as' => 'register.save',
    'uses' => 'HomeController@saveRegister'
]);

Route::get('/detail/{id}', [
    'as' => 'detail',
    'uses' => 'HomeController@detail'
]);

Route::get('/profile/{id}', [
    'as' => 'profile',
    'uses' => 'HomeController@profile'
]);
Route::get('/gallery', [
    'as' => 'gallery',
    'uses' => 'GalleryController@index'
]);
Route::get('/gallery/detail/{id}', [
    'as' => 'gallery.detail',
    'uses' => 'GalleryController@galleryDetail'
]);

Route::get('/prize', [
    'as' => 'prize',
    'uses' => 'PrizeController@index'
]);
Route::get('/how-to', [
    'as' => 'how-to',
    'uses' => 'HowToController@index'
]);

Route::get('/test', [
    'as' => 'test',
    'uses' => 'BaseController@test'
]);
Route::post('/test/save', [
    'as' => 'test.save',
    'uses' => 'BaseController@testSave'
]);
Route::group(array('prefix' => '/', 'before' => 'login'), function () {
    Route::get('/upload', [
        'as' => 'upload',
        'uses' => 'HomeController@upload'
    ]);
    Route::post('/upload/api', [
        'as' => 'upload.api',
        'uses' => 'HomeController@uploadApi'
    ]);
    Route::post('/upload/save', [
        'as' => 'upload.save',
        'uses' => 'HomeController@uploadSave'
    ]);
    Route::get('/success/{id}', [
        'as' => 'success',
        'uses' => 'HomeController@success'
    ]);
    Route::get('/log-out', [
        'as' => 'log-out',
        'uses' => 'HomeController@logout'
    ]);
});