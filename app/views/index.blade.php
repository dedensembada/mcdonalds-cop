<!DOCTYPE html>
<html lang="en">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{$page_title}}</title>
        <link rel="icon" href="/frontend/img/favicon.png" type="image/png" sizes="32x32">
        <!-- for Google -->
        <meta name="description" content="Siap jadi bagian dari serunya Olimpiade Brazil 2016! Upload segera momen main seru si kecil dan jadilah perwakilan dari Indonesia!">
        <meta name="author" content="McDonald's Indonesia">
        <meta name="keywords" content="Champions of Play McDonald's Indonesia, COP, Champions of Play">

        <meta property="fb:app_id" content="370801453114507" />
        <meta property="og:type" content="website" />
        <meta property="og:url" content="{{url()}}" />
        <meta property="og:title" content="Champions of Play McDonald's Indonesia" />
        <meta property="og:image" content="http://mcdonaldsredball.com/frontend/img/champions-of-play.jpg" />
        <meta property="og:description" content="Siap jadi bagian dari serunya Olimpiade Brazil 2016! Upload segera momen main seru si kecil dan jadilah perwakilan dari Indonesia!" />

        <link rel="canonical" href="{{url()}}" />

        <link href='https://fonts.googleapis.com/css?family=Signika' rel='stylesheet' type='text/css'>
        <link href="/frontend/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="/frontend/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="/frontend/slick/slick-theme.css"/>
        <link rel="stylesheet" type="text/css" href="/frontend/css/hover.css"/>
        <link rel="stylesheet" type="text/css" href="/frontend/css/bootstrap-datepicker.css"/>

        <!-- Add fancyBox -->
        <link rel="stylesheet" href="/frontend/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
        <link rel="stylesheet" href="/frontend/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
        <link rel="stylesheet" href="/frontend/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />

        <!-- Flow Player-->
        <link rel="stylesheet" href="/frontend/flowplayer/skin/minimalist.css">

        <link href="/frontend/css/style.css" rel="stylesheet">
    	<!-- Facebook Pixel Code -->
		<script>
		!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
		n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
		document,'script','//connect.facebook.net/en_US/fbevents.js');

		fbq('init', '949337538483751');
		fbq('track', "PageView");</script>
		<noscript><img height="1" width="1" style="display:none"
		src="https://www.facebook.com/tr?id=949337538483751&ev=PageView&noscript=1"
		/></noscript>
		<!-- End Facebook Pixel Code -->
    </head>
<body>
@include('header')
<!-- HOME -->
<div class="container" id="home-mobile">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-7 col-lg-7 text-center nopadding">
			<div class="kv" data-toggle="modal" data-target="#videoHomeModal">
				<img src="/frontend/img/kv-cop-mobile.png">
			</div>
		</div>
		<div class="col-xs-12 colx-sm-12 col-md-5 col-lg-5 text-center">
			<div class="desc">
				<h5>Siap jadi bagian dari serunya<br><span><strong>Olimpiade Brazil 2016!</strong></span><br>Upload segera momen main seru si kecil dan jadilah perwakilan dari Indonesia!</h5>
			</div>
			<br>
			<a href="{{route('register')}}"><button class="btn btn-green">Registrasi</button></a>
			@if(!isset(\Session::get('users')['id']))
			<br>- Atau -<br>
			<a href="{{route('login')}}" class="btn btn-blue">Masuk dengan Facebook</a>
			@endif
		</div>
	</div>
</div>

<div class="container" id="home-desktop">
	<div class="row">
		<br><br>
		<div class="col-xs-12 col-sm-12 col-md-7 col-lg-7 text-center">
			<div class="kv" data-toggle="modal" data-target="#videoHomeModal">
				<img src="/frontend/img/kv-cop.png" class="kv-cop-desktop">
			</div>
		</div>
		<div class="col-xs-12 colx-sm-12 col-md-5 col-lg-5 text-center">
			<br><br><br><br>
			<div class="desc">
				<h3>Siap jadi bagian dari serunya<br><span><strong>Olimpiade Brazil 2016!</strong></span><br>Upload segera momen main seru si kecil dan jadilah perwakilan dari Indonesia!</h3>
			</div>
			<br>
			<a href="{{route('register')}}"><button class="btn btn-green">Registrasi</button></a>
			@if(!isset(\Session::get('users')['id']))
			&nbsp;- Atau -&nbsp;<a href="{{route('login')}}" class="btn btn-blue">Masuk dengan Facebook</a>
			@endif
		</div>
	</div>
	<div class="clear"></div>
</div>


@include('modal')
@include('js-footer')
@include('footer')