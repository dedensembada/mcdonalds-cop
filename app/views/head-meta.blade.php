<!DOCTYPE html>
<html lang="en">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{$page_title}}</title>
        <link rel="icon" href="/frontend/img/favicon.png" type="image/png" sizes="32x32">
        <!-- for Google -->
        <meta name="description" content="Siap jadi bagian dari serunya Olimpiade Brazil 2016! Upload segera momen main seru si kecil dan jadilah perwakilan dari Indonesia!">
        <meta name="author" content="McDonald's Indonesia">
        <meta name="keywords" content="Champions of Play McDonald's Indonesia, COP, Champions of Play">

        <meta property="fb:app_id" content="370801453114507" />
        <meta property="og:type" content="website" />
        <meta property="og:url" content="{{url()}}" />
        <meta property="og:title" content="Champions of Play McDonald's Indonesia" />
        <meta property="og:image" content="http://mcdonaldsredball.com/frontend/img/champions-of-play.jpg" />
        <meta property="og:description" content="Siap jadi bagian dari serunya Olimpiade Brazil 2016! Upload segera momen main seru si kecil dan jadilah perwakilan dari Indonesia!" />

        <link rel="canonical" href="{{url()}}" />

        <link href='https://fonts.googleapis.com/css?family=Signika' rel='stylesheet' type='text/css'>
        <link href="/frontend/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="/frontend/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="/frontend/slick/slick-theme.css"/>
        <link rel="stylesheet" type="text/css" href="/frontend/css/hover.css"/>
        <link rel="stylesheet" type="text/css" href="/frontend/css/bootstrap-datepicker.css"/>

        <!-- Add fancyBox -->
        <link rel="stylesheet" href="/frontend/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
        <link rel="stylesheet" href="/frontend/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
        <link rel="stylesheet" href="/frontend/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />

        <!-- Flow Player-->
        <link rel="stylesheet" href="/frontend/flowplayer/skin/minimalist.css">

        <link href="/frontend/css/style.css" rel="stylesheet">
    </head>
<body>