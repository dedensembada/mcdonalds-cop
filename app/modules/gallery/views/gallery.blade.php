@extends("layout::main-layout")

@section("content")
    <div class="row">
        <div class="col-md-12">
            <!-- START panel -->
            <div class="panel panel-default" id="toolbar-showcase">
                <!-- panel toolbar wrapper -->
                <div class="panel-toolbar-wrapper pl10 pr10 pt5 pb5">
                    <div class="panel-toolbar text-left">
                        <div class="btn-group">
                        </div>
                    </div>
                    <div class="panel-toolbar text-right">

                        <a href="{{route('gallery-video.export')}}" class="btn btn-sm" style="background-color: #5cb85c; color: #FFF"><i class="ico-file-excel"></i> Export</a>
                    </div>
                </div>
                <!--/ panel toolbar wrapper -->

                <!-- panel body with collapse capabale -->

                <table class="table table-bordered table-hover tablesorter">
                    <thead>
                    <tr>
                        <th>No.</th>
                        <th>User Id</th>
                        <th>Title</th>
                        <th>Caption</th>
                        <th>Video</th>

                        <th>Date</th>
                        <th width="250px">Tools</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $key => $val)
                        <tr>
                            <td>{{$data->toArray()["from"]+$key}}</td>
                            <td>{{$val->user_id}}</td>
                            <td>{{$val->title}}</td>
                            <td>{{$val->caption}}</td>
                            <td>{{$val->video}}</td>
                            <td>{{$val->created_at}}</td>
                            <td class="text-center">
                                <div class="btn-group">
                                    @if($val->published==1)
                                        <a href="{{route("gallery-video.active",$val->id)}}" class="btn btn-sm btn-success"><i class="ico-check"></i> Active</a>

                                    @else
                                        <a href="{{route("gallery-video.inactive",$val->id)}}" class="btn btn-sm btn-warning"><i class="ico-check"></i> Inactive</a>


                                    @endif
                                    <a href="{{route("gallery-video.delete",$val->id)}}" class="btn btn-sm btn-danger" onclick="return confirm('Are you sure you want to delete this item?');"><i class="ico-trash"></i> Delete</a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <div class="panel-footer">
                    <div class="col-sm-6">
                        <div class=" paging_bootstrap">
                            {{$data->links()}}
                        </div>
                    </div>
                </div>
            </div>
            <!--/ panel body with collapse capabale -->
        </div>
    </div>

@stop
@section("stylesheet_header")
    <link rel="stylesheet" href="/plugins/tablesorter/css/tablesorter.css">
@stop

@section("javascript_footer")
    <script type="text/javascript" src="/plugins/tablesorter/js/tablesorter.js"></script>
    <script type="application/javascript">
        $('table').tablesorter({});
    </script>
@stop