<?php
namespace App\Modules\Dashboard\Controllers;

use Illuminate\Support\Facades\View;


class DashboardController extends \BaseController {

    public function __construct()
    {
       	$this->setupData(); 
    }

    public function dashboard(){
        $data=[];

        return View::make("dashboard::home", array(
            'page_title'    => 'Home',
            'menu'          => "menu",
            'data'          => $data

        ));
    }
}