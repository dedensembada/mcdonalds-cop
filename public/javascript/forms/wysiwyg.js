/*! ========================================================================
 * wysiwyg.js
 * Page/renders: forms-wysiwyg.html
 * Plugins used: summernote
 * ======================================================================== */
$(function () {
    // Summernote
    // ================================
    $(".summernote").summernote({
        height: 400,
        placeholder: "Input Your Text",
        fontNames: [
            'Arial', 'Arial Black', 'Comic Sans MS', 'Courier New',
            'Helvetica Neue', 'Impact', 'Lucida Grande','Roboto Slab',
            'Tahoma', 'Times New Roman', 'Verdana'
        ]
    });
});