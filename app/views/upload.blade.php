@include('head-meta')
@include('header')
<div class="container">
  <div class="row">
    <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 text-center">
      <div class="olympic-playground-mobile">
        <img src="/frontend/img/olympic-playground.png">
        <br><br>
      </div>
    </div>
  </div>
  
  <div class="row">
    <div class="col-lg-8 col-lg-offset-2" id="red-panel">
      <div class="floating-image">
        <img src="/frontend/img/olympic-playground.png" class="olympic-playground">
        <img src="/frontend/img/cloud-1.png" class="cloud-1">
        <!-- <img src="/frontend/img/cloud-2.png" class="cloud-2"> -->
        <img src="/frontend/img/cloud-3.png" class="cloud-3">
      </div>
      <div class="panel-title">
        <h3>Upload Video</h3>
        <p>Unggah video Anda dan tunggu hingga penanda konfirmasi muncul.</p>
      </div>
      {{
           Form::open(array(
               'url'=> route('upload.save'),
               'class' => '',
               'id'=>'form',
               'files' => true
           ))
      }}
      <div class="row">
        <div class="col-lg-4">
          <img src="/frontend/img/upload-video-thumbnail.jpg" class="upload-video-thumbnail">
        </div>
        <div class="col-lg-8">
          @if($errors->has())
            <div class="form-group">
              <div class="alert alert-danger fade in mt10">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4 class="semibold">Oh snap! You got an error!</h4>
                <p class="mb10">
                  {{ HTML::ul($errors->all()) }}
                </p>
              </div>
            </div>
          @endif
            <div class="form-group">
              <label>Judul Video:</label>
              <input type="text" id="title" class="form-control" placeholder="Tuliskan Judul Video Kamu..." name="title" value="{{Input::old("title")}}" required/>
            </div>
            
            <div class="form-group">
              <label>Upload Video:</label>
              <div id="loading"><img src="/frontend/img/loader.gif"></div>

              <input type="file" name="file" accept="video/mp4,video/avi,video/mov,video/x-m4v,video/*">Pilih File</input>
              <input type="hidden" name="file-video" id="file-video"/>
              <input type="hidden" name="ext" id="ext" />

              <br />
              <label>(*Format .mp4 .avi .MOV, durasi maks 1 menit, ukuran maks 100 MB)</label>
            </div>

        </div>
      </div>
      <div class="row">
        <div class="col-lg-12 text-center">
          <div class="form-group text-center">
              <a id="uploadfiles" href="javascript:;" class="btn btn-yellow">Lanjut</a>
          </div>
        </div>
      </div>
      {{ Form::close() }}
    </div>
  </div>

  <div class="step">
    <div class="row">
      <div class="col-xs-12 col-lg-6 col-lg-offset-3 text-center">
        <div class="row">
          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"><span class="done"><img src="/frontend/img/icon-done.png"></span><br><br>Registrasi<img src="/frontend/img/dot.png" class="dot"></div>
          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 active"><span class="active">2</span><br><br>Kirim Video<img src="/frontend/img/dot.png" class="dot"></div>
          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"><span>3</span><br><br>Selesai</div>
        </div>
      </div>
    </div>
  </div>

</div>
<div class="clear"></div>

@include('js-footer')
<script type="text/javascript" src="<?php echo asset('plugins/validate/jquery-validate.min.js');?>"></script>
<script type="text/javascript">
  $("#form").validate({
    rules: {
      title: {
        required: true
      }
    }
  });
  $(document).ready(function(){

    var URL = "{{route('upload.api')}}";
    $("#loading").hide();

      $('#uploadfiles').click(function () {
        if($("#form").valid()) {
        var formData = new FormData($("#form")[0]);
        $.ajax({
          type: "POST",
          url: URL,
          data: formData,
          beforeSend: function () {
          },
          contentType: false,
          processData: false,
          error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.responseText);
            alert(thrownError);
          },
          xhr: function () {
            var xhr = new window.XMLHttpRequest();
            //Upload progress
            xhr.upload.addEventListener("progress", function (evt) {
              if (evt.lengthComputable) {
                var percentComplete = evt.loaded / evt.total;
                //Do something with upload progress
                // console.log(percentComplete);
              }
            }, false);
            //Download progress
            xhr.addEventListener("progress", function (evt) {
              if (evt.lengthComputable) {
                var percentComplete = evt.loaded / evt.total;
                //Do something with download progress
                // console.log(percentComplete);
              }
            }, false);
            return xhr;
          },
          beforeSend: function () {
            $('#loading').show();
          },
          complete: function () {
          },
          success: function (json) {
            $('#loading').hide();
            $('#file-video').val(json.file);
            $('#ext').val(json.ext);
            $('#form').submit();
          }
        });
        }
      });

  });
  // $(function() {
  // $('#phone').on('keydown', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
  // })
</script>
<script type="text/javascript">


  {{--// Custom example logic--}}

  {{--var uploader = new plupload.Uploader({--}}
    {{--runtimes : 'html5,flash,silverlight,html4',--}}
    {{--browse_button : 'pickfiles', // you can pass an id...--}}
    {{--container: document.getElementById('container'), // ... or DOM Element itself--}}
    {{--url : '{{route('upload.api')}}',--}}
    {{--flash_swf_url : '/plupload/Moxie.swf',--}}
    {{--silverlight_xap_url : '/plupload/Moxie.xap',--}}
    {{--multipart: true,--}}
    {{--multipart_params: {--}}
      {{--'title': $('title').val(), // use filename as a key--}}
    {{--},--}}

    {{--file_data_name: 'file',--}}
    {{--filters : {--}}
      {{--max_file_size : '100mb',--}}
      {{--mime_types: [--}}
        {{--{title : "Image files", extensions : "mp4,mov,avi,3gp,wmv,flv"},--}}
{{--//        {title : "Zip files", extensions : "zip"}--}}
      {{--]--}}
    {{--},--}}

    {{--init: {--}}
      {{--PostInit: function() {--}}
        {{--document.getElementById('filelist').innerHTML = '';--}}

        {{--document.getElementById('uploadfiles').onclick = function() {--}}
          {{--uploader.start();--}}
          {{--return false;--}}
        {{--};--}}
      {{--},--}}

      {{--FilesAdded: function(up, files) {--}}
        {{--var max_files = 1;--}}
        {{--plupload.each(files, function(file) {--}}
          {{--if (up.files.length > max_files) {--}}
            {{--up.removeFile(file);--}}
          {{--}else{--}}
            {{--$("#filelist").html('<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>');--}}
          {{--}--}}

        {{--});--}}
      {{--},--}}

      {{--UploadProgress: function(up, file) {--}}
        {{--$("#loading").html('<span>' + file.percent + "%</span>");--}}
      {{--},--}}

      {{--UploadComplete: function(up, file, info){--}}
        {{--console.log(up);--}}
        {{--console.log(file);--}}
        {{--console.log(info);--}}
        {{--$('#form-upload').submit();--}}
      {{--},--}}
      {{--Error: function(up, err) {--}}
        {{--console.log("\nError #" + err.code + ": " + err.message);--}}
      {{--}--}}
{{--//      QueueChanged: function(up) {--}}
{{--//        if(uploader.files.length > 1)--}}
{{--//        {--}}
{{--//          uploader.files.reverse().splice(1, uploader.files.length);--}}
{{--//        }--}}
{{--//        uploader.refresh();--}}
{{--//      }--}}
    {{--}--}}
  {{--});--}}

  {{--uploader.init();--}}

</script>
@include('footer')
