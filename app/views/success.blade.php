<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#  article: http://ogp.me/ns/article#">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{$page_title}}</title>
    <link rel="icon" href="/frontend/img/favicon.png" type="image/png" sizes="32x32">
    <!-- for Google -->
    <meta name="description" content="Champions of Play McDonald's Indonesia">
    <meta name="author" content="McDonald's Indonesia">
    <meta name="keywords" content="Siap jadi bagian dari serunya Olimpiade Brazil 2016! Upload segera momen main seru si kecil dan jadilah perwakilan dari Indonesia!">

    <meta property="fb:app_id" content="370801453114507" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="http://mcdonaldsredball.com/" />
    <meta property="og:title" content="{{(!empty($data['title']))?$data["title"]:$data["title"]}}" />
    <meta property="og:image:height"  content="315" />
    <meta property="og:image:width"  content="600" />
    <meta property="og:description" content="Anakku berkesempatan ke Olimpiade 2016 di Brazil dengan ikutan Champions of Play McDonald's" />
    <meta property="og:image" content="{{(!empty($data["image"]))?'https://s3-ap-southeast-1.amazonaws.com/video.mcdonaldsredball.com/thumbnail/'.$data["image"]:'http://mcdonaldsredball.com/frontend/img/champions-of-play.jpg'}}" />
    <meta property="og:site_name" content="McDonald's Indonesia" />
    <meta property="article:author" content="{{url()}}" />
    <meta property="profile:first_name" content="McDonald's Indonesia" />

    <link rel="canonical" href="{{route("detail",array($data["id"]))}}" />

    <link href='https://fonts.googleapis.com/css?family=Signika' rel='stylesheet' type='text/css'>
    <link href="/frontend/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/frontend/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="/frontend/slick/slick-theme.css"/>
    <link rel="stylesheet" type="text/css" href="/frontend/css/hover.css"/>

    <!-- Add fancyBox -->
    <link rel="stylesheet" href="/frontend/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
    <link rel="stylesheet" href="/frontend/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
    <link rel="stylesheet" href="/frontend/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />

    <!-- Flow Player-->
    <link rel="stylesheet" href="/frontend/flowplayer/skin/minimalist.css">

    <link href="/frontend/css/style.css" rel="stylesheet">

    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','//connect.facebook.net/en_US/fbevents.js');

    fbq('init', '949337538483751');
    fbq('track', "CompleteRegistration");</script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=949337538483751&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->
</head>
<body>
@include('header')
<div class="container">
  <div class="row">
    <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 text-center">
      <div class="olympic-playground-mobile">
        <img src="/frontend/img/olympic-playground.png">
        <br><br>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-8 col-lg-offset-2" id="red-panel">
      <div class="floating-image">
        <img src="/frontend/img/olympic-playground.png" class="olympic-playground">
        <img src="/frontend/img/cloud-1.png" class="cloud-1">
        <img src="/frontend/img/cloud-2.png" class="cloud-2">
        <img src="/frontend/img/cloud-3.png" class="cloud-3">
      </div>
      <div class="panel-title">
        <br><br>
        <h3>Selesai</h3>
        <p>Semoga Anda terpilih sebagai  perwakilan Indonesia di Olimpiade Brazil 2016!<br>Anda kini juga dapat membagi hasil unggahan Anda ke akun social media yang dikehendaki.</p>
      </div>
      <div class="flowplayer text-center">
        <!-- <img src="https://s3-ap-southeast-1.amazonaws.com/video.mcdonaldsredball.com/thumbnail/{{$data['image']}}"> -->
        <img src="/frontend/img/champions-of-play.jpg">
        
      </div>
      <div class="panel-title">
        <h3>{{$data['title']}}</h3>
        <p>oleh: {{ \User::find($data['user_id'])['name']}}</p>
      </div>
      <div class="row">
        <div class="col-lg-5 col-lg-offset-1 text-center">
          <div class="form-group text-center">
            <button onclick="window.open('https://twitter.com/intent/tweet?text=Anakku berkesempatan ke Olimpiade 2016 di Brazil dengan ikutan Champions of Play McDonald&#8217s&url=http://mcdonaldsredball.com/','twitter-share-dialog','width=auto,height=auto');return false;" type="button" class="btn btn-twitter">Bagikan di Twitter</button>
          </div>
        </div>
        <div class="col-lg-5 text-center">
          <div class="form-group text-center">
            <button onclick="window.open('https://www.facebook.com/sharer/sharer.php?u={{route("detail",array($data["id"]))}}','facebook-share-dialog','width=auto,height=auto');return false;" type="button" class="btn btn-facebook">Bagikan di Facebook</button>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="step">
    <div class="row">
      <div class="col-xs-12 col-lg-6 col-lg-offset-3 text-center">
        <div class="row">
          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"><span class="done"><img src="/frontend/img/icon-done.png"></span><br><br>Registrasi<img src="/frontend/img/dot.png" class="dot"></div>
          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4"><span class="done"><img src="/frontend/img/icon-done.png"></span><br><br>Kirim Video<img src="/frontend/img/dot.png" class="dot"></div>
          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 active"><span class="active">3</span><br><br>Selesai</div>
        </div>
      </div>
    </div>
  </div>

</div>
<div class="clear"></div>
@include('js-footer')
@include('footer')
